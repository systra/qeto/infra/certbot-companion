## Pre-Hooks and Post-Hooks

The Pre- and Post-Hooks of [certbot](https://certbot.eff.org/docs) are available through the corresponding environment variables. This allows to trigger actions just before and after certificates are issued (see [certbot documentation](https://eff-certbot.readthedocs.io/en/latest/using.html#pre-and-post-validation-hooks)).

If you set `CERTBOT_PRE_HOOK` and/or `CERTBOT_POST_HOOK` on the **certbot-companion** container, **the actions for all certificates will be the same**. If you want specific actions to be run for specific certificates, set the `CERTBOT_PRE_HOOK` / `CERTBOT_POST_HOOK` environment variable(s) on the proxied container(s) instead. Default (on the **certbot-companion** container) and per-container `CERTBOT_PRE_HOOK` / `CERTBOT_POST_HOOK` environment variables aren't combined: if both default and per-container variables are set for a given proxied container, the per-container variables will take precedence over the default.

If you want to run the same default hooks for most containers but not for some of them, you can set the `CERTBOT_PRE_HOOK` / `CERTBOT_POST_HOOK` environment variables to the Bash noop operator (ie, `CERTBOT_PRE_HOOK=:`) on those containers.

#### Pre-Hook: `CERTBOT_PRE_HOOK`
This command will be run before certificates are issued.

For example `echo 'start'` on the **certbot-companion** container (setting a default Pre-Hook):
```shell
$ docker run --detach \
    --name nginx-proxy-certbot \
    --volumes-from nginx-proxy \
    --volume /var/run/docker.sock:/var/run/docker.sock:ro \
    --volume certbot:/etc/certbot \
    --env "DEFAULT_EMAIL=mail@yourdomain.tld" \
    --env "CERTBOT_PRE_HOOK=echo 'start'" \
    registry.gitlab.com/systra/qeto/infra/certbot-companion
```

And on a proxied container (setting a per-container Pre-Hook):
```shell
$ docker run --detach \
    --name your-proxyed-app \
    --env "VIRTUAL_HOST=yourdomain.tld" \
    --env "LETSENCRYPT_HOST=yourdomain.tld" \
    --env "CERTBOT_PRE_HOOK=echo 'start'" \
    nginx
```

#### Post-Hook: `CERTBOT_POST_HOOK`
This command will be run after certificates are issued.

For example `echo 'end'` on the **certbot-companion** container (setting a default Post-Hook):
```shell
$ docker run --detach \
    --name nginx-proxy-certbot \
    --volumes-from nginx-proxy \
    --volume /var/run/docker.sock:/var/run/docker.sock:ro \
    --volume certbot:/etc/certbot \
    --env "DEFAULT_EMAIL=mail@yourdomain.tld" \
    --env "CERTBOT_POST_HOOK=echo 'end'" \
    registry.gitlab.com/systra/qeto/infra/certbot-companion
```

And on a proxied container (setting a per-container Post-Hook):
```shell
$ docker run --detach \
    --name your-proxyed-app \
    --env "VIRTUAL_HOST=yourdomain.tld" \
    --env "LETSENCRYPT_HOST=yourdomain.tld" \
    --env "CERTBOT_POST_HOOK=echo 'start'" \
    nginx
```

#### Verification:
If you want to check wether the hook-command is delivered properly to [certbot](https://eff-certbot.readthedocs.io/en/latest/using.html#pre-and-post-validation-hooks), you should check `/etc/certbot/[EMAILADDRESS]/renewal/[DOMAIN].conf`.
The variable `pre_hook` contains the Pre-Hook-Command.
The variable `post_hook` contains the Pre-Hook-Command.

#### Limitations
* The commands that can be used in the hooks are limited to the commands available inside the **certbot-companion** container. `curl` and `wget` are available, therefore it is possible to communicate with tools outside the container via HTTP, allowing for complex actions to be implemented outside or in other containers.

#### Use-cases
* Changing some firewall rules just for the ACME authorization, so the ports 80 and/or 443 don't have to be publicly reachable at all time.
* Certificate "post processing" / conversion to another format.
* Monitoring.

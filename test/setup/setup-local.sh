#!/bin/bash
set -e

function get_environment {
  dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  LOCAL_BUILD_DIR="$(cd "$dir/../.." && pwd)"
  export WORKSPACE="$LOCAL_BUILD_DIR"
  # shellcheck source=/dev/null
  [[ -f "${WORKSPACE}/test/local_test_env.sh" ]] && source "${WORKSPACE}/test/local_test_env.sh"
  # Get the environment variables from the test/config.sh file
  source "${LOCAL_BUILD_DIR}/test/config.sh"
  # If environment variable where sourced or manually set use them, else use those from test.conf
  export NGINX_CONTAINER_NAME="${NGINX_CONTAINER_NAME:-${CI_NGINX_CONTAINER_NAME}}"
  export TEST_DOMAINS="${TEST_DOMAINS:-${CI_TEST_DOMAINS}}"
  export NETWORK=acme_net
}

case $1 in
  --setup)
    get_environment
    # Prepare the env file that run.sh will source
    [[ -e "${WORKSPACE}/.dockerignore" ]] && mv "${WORKSPACE}/.dockerignore" "${WORKSPACE}/dockerignore.bak"
    cat > "${WORKSPACE}/test/local_test_env.sh" <<EOF
export WORKSPACE="$LOCAL_BUILD_DIR"
export NGINX_CONTAINER_NAME="$NGINX_CONTAINER_NAME"
export TEST_DOMAINS="$TEST_DOMAINS"
export NETWORK="$NETWORK"
EOF
    # Pull nginx:alpine
    docker pull nginx:alpine
    # Prepare the test setup using the setup scripts
    "${WORKSPACE}/test/setup/pebble/setup-pebble.sh"
    "${WORKSPACE}/test/setup/setup-nginx-proxy.sh"
    ;;

  --teardown)
    get_environment
    # Stop and remove nginx-proxy and (if required) docker-gen
    for cid in $(docker ps -a --filter "label=com.github.nginx-proxy.certbot-companion.test-suite" --format "{{.ID}}"); do
      docker stop -t 0 "$cid" || true
      docker rm --volumes "$cid" || true
    done
    # Stop and remove Pebble
    docker compose --file "${WORKSPACE}/test/setup/pebble/docker-compose.yml" down
    [[ -f "${WORKSPACE}/pebble.minica.pem" ]] && rm "${WORKSPACE}/pebble.minica.pem"
    # Cleanup files created by the setup
    if [[ -n "${WORKSPACE// }" ]]; then
      [[ -f "${WORKSPACE}/nginx.tmpl" ]] && rm "${WORKSPACE}/nginx.tmpl"
      rm "${WORKSPACE}/test/local_test_env.sh"
      [[ -e "${WORKSPACE}/dockerignore.bak" ]] && mv "${WORKSPACE}/dockerignore.bak" "${WORKSPACE}/.dockerignore"
    fi
    ;;

    *)
    echo "Usage:"
    echo ""
    echo "    --setup : setup the test suite."
    echo "    --teardown : remove the test suite containers, configuration and files."
    echo ""
    ;;
esac

#!/bin/bash

set -e

setup_pebble() {
    curl --silent --show-error https://raw.githubusercontent.com/letsencrypt/pebble/master/test/certs/pebble.minica.pem > "${WORKSPACE}/pebble.minica.pem"
    docker compose --file "${WORKSPACE}/test/setup/pebble/docker-compose.yml" up --detach
}

dockercurl() {
    docker run --rm --network=acme_net test-curl curl --cacert "/pebble.minica.pem" --silent --show-error "$@"
}

wait_for_pebble() {
    for endpoint in 'https://pebble:14000/dir' 'http://challtestsrv:8055'; do
        while ! dockercurl "$endpoint" >/dev/null; do
            if [ $((i * 5)) -gt $((5 * 60)) ]; then
                echo "$endpoint was not available under 5 minutes, timing out."
                exit 1
            fi
            i=$((i + 1))
            sleep 5
        done
    done
}

setup_pebble_challtestserv() {
    dockercurl --data '{"ip":"10.30.50.1"}' http://challtestsrv:8055/set-default-ipv4
    dockercurl --data '{"ip":""}' http://challtestsrv:8055/set-default-ipv6
    dockercurl --data '{"host":"lim.it", "addresses":["10.0.0.0"]}' http://challtestsrv:8055/add-a
    IFS=',' read -r -a domains <<< "$TEST_DOMAINS"
    nginx_proxy_ip="10.30.50.4"
    for d in "${domains[@]}"; do
      dockercurl --data '{"host":"'"$d"'", "addresses":["'"$nginx_proxy_ip"'"]}' http://challtestsrv:8055/add-a
      dockercurl --data '{"host":"sub.'"$d"'", "addresses":["'"$nginx_proxy_ip"'"]}' http://challtestsrv:8055/add-a
    done
}

setup_pebble
docker build -t test-curl -f- "${WORKSPACE}" >/dev/null <<EOF
FROM curlimages/curl
COPY ./pebble.minica.pem /pebble.minica.pem
EOF
trap 'docker image rm -f test-curl >/dev/null 2>&1' EXIT TERM
wait_for_pebble
setup_pebble_challtestserv
docker -D compose --file "${WORKSPACE}/test/setup/pebble/docker-compose.yml" logs

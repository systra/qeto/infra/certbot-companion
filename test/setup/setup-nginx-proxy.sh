#!/bin/bash
set -e
IFS=',' read -r -a domains <<< "$TEST_DOMAINS"
declare -a hostmapping
for d in "${domains[@]}"; do
  hostmapping+=("--add-host=$d:127.0.0.1" "--add-host=sub.$d:127.0.0.1")
done
docker run -d \
  --name "$NGINX_CONTAINER_NAME" \
  -v /etc/nginx/certs \
  -v /etc/nginx/conf.d \
  -v /etc/nginx/vhost.d \
  -v /usr/share/nginx/html \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  --label com.github.nginx-proxy.certbot-companion.test-suite \
  --network "$NETWORK" \
  "${hostmapping[@]}" \
  nginxproxy/nginx-proxy

#!/bin/bash
## Test for DNS-01 challenge certificate.

le_container_name="$(basename "${0%/*}")_$(date "+%Y-%m-%d_%H.%M.%S")"
cat > "${WORKSPACE}"/pebble-auth.sh <<'EOF'
#!/bin/bash
curl --silent --show-error --data '{"host":"_acme-challenge.'${CERTBOT_DOMAIN}'.", "value": "'${CERTBOT_VALIDATION}'"}' http://challtestsrv:8055/set-txt
EOF
cat > "${WORKSPACE}"/pebble-cleanup.sh <<'EOF'
#!/bin/bash
curl --silent --show-error --data '{"host":"_acme-challenge.'${CERTBOT_DOMAIN}'."}' http://challtestsrv:8055/clear-txt
EOF
chmod +x "${WORKSPACE}"/pebble-*.sh
run_le_container "${1:?}" "$le_container_name" \
    --cli-args "--env ACME_CHALLENGE=dns-01" \
    --cli-args "--env ACME_DNS_PARAMS=--preferred-challenges dns --manual --manual-auth-hook /hooks/auth.sh --manual-cleanup-hook /hooks/cleanup.sh" \
    --cli-args "--volume ${WORKSPACE}/pebble-auth.sh:/hooks/auth.sh:ro" \
    --cli-args "--volume ${WORKSPACE}/pebble-cleanup.sh:/hooks/cleanup.sh:ro"
# Create the $domains array from comma separated domains in TEST_DOMAINS.
IFS=',' read -r -a domains <<< "$TEST_DOMAINS"

# Cleanup function with EXIT trap
function cleanup {
  # Remove any remaining Nginx container(s) silently.
  docker rm --force "${domains[@]}" &> /dev/null
  # Cleanup the files created by this run of the test to avoid foiling following test(s).
  docker exec "$le_container_name" /app/cleanup_test_artifacts
  # Stop the LE container
  docker stop "$le_container_name" > /dev/null
  docker container prune -f > /dev/null
  rm -f "${WORKSPACE}"/pebble-*.sh &> /dev/null
}
trap cleanup EXIT TERM

# Run a separate nginx container for each domain in the $domains array.
# Start all the containers in a row so that docker-gen debounce timers fire only once.
for domain in "${domains[@]}"; do
  run_nginx_container --hosts "$domain"
done
nginx_proxy_ip=$(docker inspect "$NGINX_CONTAINER_NAME"|jq -r ".[].NetworkSettings.Networks.${NETWORK}.IPAddress")
for domain in "${domains[@]}"; do
  # Wait for a symlink at /etc/nginx/certs/$domain.crt
  if ! wait_for_symlink "$domain" "$le_container_name" "./${domain}/fullchain.pem" ; then
    continue
  fi
  # then grab the certificate in text form from the file ...
  created_cert="$(docker exec "$le_container_name" \
    openssl x509 -in "/etc/nginx/certs/${domain}/cert.pem" -text -noout)"
  # ... as well as the certificate fingerprint.
  created_cert_fingerprint="$(docker exec "$le_container_name" \
    openssl x509 -in "/etc/nginx/certs/${domain}/cert.pem" -fingerprint -noout)"
  # Check if the domain is on the certificate.
  if ! grep -q "$domain" <<< "$created_cert"; then
    echo "Domain $domain isn't on certificate."
    continue
  fi
  [[ "${DRY_RUN:-}" == 1 ]] && echo "Domain $domain is on certificate."
  # Wait for a connection to https://domain then grab the served certificate fingerprint.
  if ! wait_for_conn --domain "$domain"; then
    continue
  fi
  served_cert_fingerprint="$(echo \
    | docker run --rm -i "--network=$NETWORK" frapsoft/openssl s_client -showcerts -servername "$domain" -connect "$nginx_proxy_ip:443" 2>/dev/null \
    | openssl x509 -fingerprint -noout -in /dev/stdin)"
  # Compare fingerprints from the cert on file and what we got from the https connection.
  # If not identical, display a full diff.
  if [ "$created_cert_fingerprint" != "$served_cert_fingerprint" ]; then
    echo "Nginx served an incorrect certificate for $domain."
    served_cert="$(echo \
      | docker run --rm -i "--network=$NETWORK" frapsoft/openssl s_client -showcerts -servername "$domain" -connect "$nginx_proxy_ip:443" 2>/dev/null \
      | openssl x509 -text -noout -in /dev/stdin \
      | sed 's/ = /=/g' )"
    diff -u <(echo "${created_cert// = /=}") <(echo "$served_cert")
  elif [[ "${DRY_RUN:-}" == 1 ]]; then
    echo "The correct certificate for $domain was served by Nginx."
  fi
done

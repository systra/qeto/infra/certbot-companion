#!/bin/bash

## Test for Certbot accounts handling.
le_container_name="$(basename "${0%/*}")_$(date "+%Y-%m-%d_%H.%M.%S")"
default_email="toto@example.com"
run_le_container "${1:?}" "$le_container_name"
# Create the $domains array from comma separated domains in TEST_DOMAINS.
IFS=',' read -r -a domains <<< "$TEST_DOMAINS"
# Cleanup function with EXIT trap
function cleanup {
  # Remove any remaining Nginx container(s) silently.
  for domain in "${domains[@]}"; do
    docker rm --force "$domain" &> /dev/null
  done
  # Cleanup the files created by this run of the test to avoid foiling following test(s).
  docker exec "$le_container_name" /app/cleanup_test_artifacts
  # Stop the LE container
  docker stop "$le_container_name" > /dev/null
  docker container prune -f > /dev/null
}
trap cleanup EXIT TERM

# Run an nginx container for ${domains[0]}.
run_nginx_container --hosts "${domains[0]}"
# Wait for a symlink at /etc/nginx/certs/${domains[0]}.crt
wait_for_symlink "${domains[0]}" "$le_container_name"
# Hard set the account dir based on the test ACME CA used.
account_dir="pebble:14000/dir"
uri_base="https://pebble:14000/my-account/"
if docker exec "$le_container_name" test '!' -d "/etc/certbot/${default_email}"; then
  echo "The /etc/certbot/${default_email} folder does not exist."
fi
json_regr_file=$(docker exec "$le_container_name" find "/etc/certbot/${default_email}/accounts/$account_dir" -name 'regr.json')
if [[ -z "$json_regr_file" ]]; then
  echo "The regr.json file does not exist in /etc/certbot/${default_email}/accounts/$account_dir."
elif [[ ! "$(docker exec "$le_container_name" jq -r .uri "$json_regr_file")" =~ ^${uri_base}[0-9]+ ]]; then
  echo "Uri does not match ${uri_base}[0-9]+ in ${json_regr_file}."
  docker exec "$le_container_name" jq . "$json_regr_file"
fi
# Stop the nginx and companion containers silently.
docker stop "${domains[0]}" &> /dev/null
docker stop "$le_container_name" &> /dev/null
# Run the companion container with the DEFAULT_EMAIL env var set.
default_email="contact@${domains[1]}"
le_container_name="${le_container_name}_default"
run_le_container "${1:?}" "$le_container_name" "--env DEFAULT_EMAIL=${default_email}"
# Run an nginx container for ${domains[1]} without LETSENCRYPT_EMAIL set.
run_nginx_container --hosts "${domains[1]}"
# Wait for a symlink at /etc/nginx/certs/${domains[1]}.crt
wait_for_symlink "${domains[1]}" "$le_container_name"
# Test if the expected folder / file / content are there.
if docker exec "$le_container_name" test '!' -d "/etc/certbot/${default_email}"; then
  echo "The /etc/certbot/${default_email} folder does not exist."
fi
json_regr_file=$(docker exec "$le_container_name" find "/etc/certbot/${default_email}/accounts/$account_dir" -name 'regr.json')
if [[ -z "$json_regr_file" ]]; then
  echo "The regr.json file does not exist in /etc/certbot/${default_email}/accounts/$account_dir."
elif [[ ! "$(docker exec "$le_container_name" jq -r .uri "$json_regr_file")" =~ ^${uri_base}[0-9]+ ]]; then
  echo "Uri does not match ${uri_base}[0-9]+ in ${json_regr_file}."
  docker exec "$le_container_name" jq . "$json_regr_file"
fi
# Run an nginx container for ${domains[2]} with LETSENCRYPT_EMAIL set.
container_email="contact@${domains[2]}"
run_nginx_container --hosts "${domains[2]}" --cli-args "--env LETSENCRYPT_EMAIL=${container_email}"
# Wait for a symlink at /etc/nginx/certs/${domains[2]}.crt
wait_for_symlink "${domains[2]}" "$le_container_name"
# Test if the expected folder / file / content are there.
if docker exec "$le_container_name" test '!' -d "/etc/certbot/${container_email}"; then
  echo "The /etc/certbot/${container_email} folder does not exist."
fi
json_regr_file=$(docker exec "$le_container_name" find "/etc/certbot/${container_email}/accounts/$account_dir" -name 'regr.json')
if [[ -z "$json_regr_file" ]]; then
  echo "The regr.json file does not exist in /etc/certbot/${container_email}/accounts/$account_dir."
elif [[ ! "$(docker exec "$le_container_name" jq -r .uri "$json_regr_file")" =~ ^${uri_base}[0-9]+ ]]; then
  echo "Uri does not match ${uri_base}[0-9]+ in ${json_regr_file}."
  docker exec "$le_container_name" jq . "$json_regr_file"
fi
# Stop the nginx containers silently.
docker stop "${domains[1]}" &> /dev/null
docker stop "${domains[2]}" &> /dev/null

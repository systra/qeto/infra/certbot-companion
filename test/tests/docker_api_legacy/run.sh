#!/bin/bash

## Test for the Docker API with legacy labels.
nginx_vol='nginx-volumes-from-legacy'
nginx_env='nginx-env-var-legacy'
nginx_lbl='nginx-label-legacy'

# Cleanup function with EXIT trap
function cleanup {
  # Kill the Docker events listener
  kill "$docker_events_pid" && wait "$docker_events_pid" 2>/dev/null
  # Remove the remaining containers silently
  docker rm --force \
    "$nginx_vol" \
    "$nginx_env" \
    "$nginx_lbl" \
    &> /dev/null
}
trap cleanup EXIT TERM

# Set the commands to be passed to docker exec
commands='source /app/functions.sh; reload_nginx > /dev/null; check_nginx_proxy_container_run; get_nginx_proxy_container'
# Listen to Docker exec_start events
docker events \
  --filter event=exec_start \
  --format 'Container {{.Actor.Attributes.name}} received {{.Action}}' &
docker_events_pid=$!
# Run a nginx-proxy container named nginx-volumes-from, without the nginx_proxy label
docker run --rm -d \
  --name "$nginx_vol" \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  nginxproxy/nginx-proxy > /dev/null
# Run a nginx-proxy container named nginx-env-var, without the nginx_proxy label
docker run --rm -d \
  --name "$nginx_env" \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  nginxproxy/nginx-proxy > /dev/null
# This should target the nginx-proxy container obtained with
# the --volume-from argument (nginx-volumes-from)
docker run --rm \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  --volumes-from "$nginx_vol" \
  "$1" \
  bash -c "$commands" 2>&1
# This should target the nginx-proxy container obtained with
# the NGINX_PROXY_CONTAINER environment variable (nginx-env-var)
docker run --rm \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  --volumes-from "$nginx_vol" \
  -e "NGINX_PROXY_CONTAINER=$nginx_env" \
  "$1" \
  bash -c "$commands" 2>&1
# Run a nginx-proxy container named nginx-label, with the nginx_proxy label.
# Store the container id in the labeled_nginx_cid variable.
labeled_nginx_cid="$(docker run --rm -d \
  --name "$nginx_lbl" \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \
  nginxproxy/nginx-proxy)"
# This should target the nginx-proxy container with the label (nginx-label)
docker run --rm \
  -v /var/run/docker.sock:/var/run/docker.sock:ro \
  --volumes-from "$nginx_vol" \
  -e "NGINX_PROXY_CONTAINER=$nginx_env" \
  "$1" \
  bash -c "$commands" 2>&1
cat > "${WORKSPACE}/test/tests/docker_api_legacy/expected-std-out.txt" <<EOF
Container $nginx_vol received exec_start: sh -c /app/docker-entrypoint.sh /usr/local/bin/docker-gen /app/nginx.tmpl /etc/nginx/conf.d/default.conf; /usr/sbin/nginx -s reload
$nginx_vol
Container $nginx_env received exec_start: sh -c /app/docker-entrypoint.sh /usr/local/bin/docker-gen /app/nginx.tmpl /etc/nginx/conf.d/default.conf; /usr/sbin/nginx -s reload
$nginx_env
Container $nginx_lbl received exec_start: sh -c /app/docker-entrypoint.sh /usr/local/bin/docker-gen /app/nginx.tmpl /etc/nginx/conf.d/default.conf; /usr/sbin/nginx -s reload
$labeled_nginx_cid
EOF

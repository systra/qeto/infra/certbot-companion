#!/bin/bash
set -e
globalTests+=(
	docker_api
	docker_api_legacy
	location_config
	default_cert
	certs_single
	certs_san
	certs_single_domain
	certs_standalone
	certs_dns
	force_renew
	certbot_accounts
	private_keys
	container_restart
	permissions_default
	permissions_custom
	symlinks
	certbot_hooks
)
export CI_NGINX_CONTAINER_NAME=test-nginx-proxy
export CI_TEST_DOMAINS=le1.wtf,le2.wtf,le3.wtf

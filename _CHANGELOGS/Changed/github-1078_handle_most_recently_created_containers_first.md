- Handle most recently created containers first (github-1078)  
  https://github.com/nginx-proxy/acme-companion/pull/1078

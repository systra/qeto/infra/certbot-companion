**certbot-companion** is a lightweight companion container for [**nginx-proxy**](https://github.com/nginx-proxy/nginx-proxy).

It handles the automated creation, renewal and use of SSL certificates for proxied Docker containers through the ACME protocol.

It’s a fork from the [`acme-companion`](https://github.com/nginx-proxy/acme-companion) repository to use `certbot` instead.

### Features:
* Automated creation/renewal of Let's Encrypt (or other ACME CAs) certificates using [**certbot**](https://eff-certbot.readthedocs.io).
* Let's Encrypt / ACME domain validation through `http-01` or `dns-01` challenges.
* Automated update and reload of nginx config on certificate creation/renewal.
* Support creation of [Multi-Domain (SAN) Certificates](./docs/Let's-Encrypt-and-Certbot.md#multi-domains-certificates).
* Creation of a strong [RFC7919 Diffie-Hellman Group](https://datatracker.ietf.org/doc/html/rfc7919#appendix-A) at startup.

### Requirements for `http-01` challenge:
* You must provide a valid **email**.
* Your host **must** be publicly reachable on **both** port [`80`](https://letsencrypt.org/docs/allow-port-80/) and [`443`](https://github.com/nginx-proxy/acme-companion/discussions/873#discussioncomment-1410225)
* Check your firewall rules and [**do not attempt to block port `80`**](https://letsencrypt.org/docs/allow-port-80/) as that will prevent `http-01` challenges from completing.
* For the same reason, you can't use nginx-proxy's [`HTTPS_METHOD=nohttp`](https://github.com/nginx-proxy/nginx-proxy#how-ssl-support-works).
* The (sub)domains you want to issue certificates for must correctly resolve to the host.
* Your DNS provider must [answer correctly to CAA record requests](https://letsencrypt.org/docs/caa/).
* If your (sub)domains have AAAA records set, the host must be publicly reachable over IPv6 on port `80` and `443`.

### Requirements for `dns-01` challenge:
* You must provide a valid **email**.
* `certbot` should have a **plugin** supporting your **DNS provider**.
* You should provide a mean to authenticate to your DNS provider. See your certbot plugin documentation.

![schema](./schema.png)

## Basic usage (with the nginx-proxy container)

Three writable volumes must be declared on the **nginx-proxy** container so that they can be shared with the **certbot-companion** container:

* `/etc/nginx/certs` to store certificates and private keys (readonly for the **nginx-proxy** container).
* `/etc/nginx/vhost.d` to change the configuration of vhosts (required so the CA may access `http-01` challenge files).
* `/usr/share/nginx/html` to write `http-01` challenge files.

Additionally, a fourth volume must be declared on the **certbot-companion** container to store `certbot` configuration and state: `/etc/certbot`.

Please also read the doc about [data persistence](./docs/Persistent-data.md).

Example of use:

### Step 1 - nginx-proxy

Start **nginx-proxy** with the three additional volumes declared:

```shell
$ docker run --detach \
    --name nginx-proxy \
    --publish 80:80 \
    --publish 443:443 \
    --volume certs:/etc/nginx/certs \
    --volume vhost:/etc/nginx/vhost.d \
    --volume html:/usr/share/nginx/html \
    --volume /var/run/docker.sock:/tmp/docker.sock:ro \
    nginxproxy/nginx-proxy
```

Binding the host docker socket (`/var/run/docker.sock`) inside the container to `/tmp/docker.sock` is a requirement of **nginx-proxy**.

### Step 2 - certbot-companion

Start the **certbot-companion** container, getting the volumes from **nginx-proxy** with `--volumes-from`:

```shell
$ docker run --detach \
    --name nginx-proxy-certbot \
    --volumes-from nginx-proxy \
    --volume /var/run/docker.sock:/var/run/docker.sock:ro \
    --volume certbot:/etc/certbot \
    --env "DEFAULT_EMAIL=mail@yourdomain.tld" \
    registry.gitlab.com/systra/qeto/infra/certbot-companion
```

The host docker socket has to be bound inside this container too, this time to `/var/run/docker.sock`.

It is **mandatory** to provide a valid default email address through the `DEFAULT_EMAIL` environment variable, so that Let's Encrypt can warn you about expiring certificates and allow you to recover your account.

For a **DNS** challenge with **Gandi** provider:

```shell
$ echo 'dns_gandi_token=XXX' > gandi.ini
$ docker run --detach \
    --name nginx-proxy-certbot \
    --volumes-from nginx-proxy \
    --volume /var/run/docker.sock:/var/run/docker.sock:ro \
    --volume $PWD/gandi.ini:/etc/gandi.ini:ro \
    --volume certbot:/etc/certbot \
    --env "DEFAULT_EMAIL=mail@yourdomain.tld" \
    --env "ACME_CHALLENGE=dns-01" \
    --env "ACME_DNS_PARAMS=--authenticator dns-gandi --dns-gandi-credentials /etc/gandi.ini" \
    registry.gitlab.com/systra/qeto/infra/certbot-companion
```

### Step 3 - proxied container(s)

Once both **nginx-proxy** and **certbot-companion** containers are up and running, start any container you want proxied with environment variables `VIRTUAL_HOST` and `LETSENCRYPT_HOST` both set to the domain(s) your proxied container is going to use.

[`VIRTUAL_HOST`](https://github.com/nginx-proxy/nginx-proxy#usage) control proxying by **nginx-proxy** and `LETSENCRYPT_HOST` control certificate creation and SSL enabling by **certbot-companion**.

Certificates will only be issued for containers that have both `VIRTUAL_HOST` and `LETSENCRYPT_HOST` variables set to domain(s) that correctly resolve to the host, provided the host is publicly reachable.

```shell
$ docker run --detach \
    --name your-proxied-app \
    --env "VIRTUAL_HOST=subdomain.yourdomain.tld" \
    --env "LETSENCRYPT_HOST=subdomain.yourdomain.tld" \
    nginx
```

The containers being proxied must expose the port to be proxied, either by using the `EXPOSE` directive in their Dockerfile or by using the `--expose` flag to `docker run` or `docker create`.

If the proxied container listen on and expose another port than the default `80`, you can force **nginx-proxy** to use this port with the [`VIRTUAL_PORT`](https://github.com/nginx-proxy/nginx-proxy#multiple-ports) environment variable.

Example using [Grafana](https://hub.docker.com/r/grafana/grafana/) (expose and listen on port 3000):

```shell
$ docker run --detach \
    --name grafana \
    --env "VIRTUAL_HOST=othersubdomain.yourdomain.tld" \
    --env "VIRTUAL_PORT=3000" \
    --env "LETSENCRYPT_HOST=othersubdomain.yourdomain.tld" \
    --env "LETSENCRYPT_EMAIL=mail@yourdomain.tld" \
    grafana/grafana
```

Repeat [Step 3](#step-3---proxied-containers) for any other container you want to proxy.

## Additional documentation

Please check the [docs section](./docs).

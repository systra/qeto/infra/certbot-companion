#!/bin/bash

# shellcheck source=functions.sh
source /app/functions.sh

CERTS_UPDATE_INTERVAL="${CERTS_UPDATE_INTERVAL:-3600}"
ACME_CA_URI="${ACME_CA_URI:-"https://acme-v02.api.letsencrypt.org/directory"}"
ACME_CA_TEST_URI="https://acme-staging-v02.api.letsencrypt.org/directory"
# https://neuromancer.sk/std/secg/
DEFAULT_KEY_SIZE="${DEFAULT_KEY_SIZE:-ec-256}"
RENEW_PRIVATE_KEYS="$(lc "${RENEW_PRIVATE_KEYS:-true}")"
ACME_CHALLENGE="${ACME_CHALLENGE:-http-01}"
if [[ ! "$ACME_CHALLENGE" =~ ^(http-01|dns-01)$ ]]; then
    echo "Warning: ACME_CHALLENGE ($ACME_CHALLENGE) is not valid (http-01 or dns-01), using default http-01 challange."
    ACME_CHALLENGE=http-01
fi
if [[ "$ACME_CHALLENGE" == "dns-01" ]] && [[ -z "$ACME_DNS_PARAMS" ]]; then
    echo "Warning: ACME_DNS_PARAMS cannot be empty when ACME_CHALLENGE is dns-01, reverting to use http-01 challange."
    ACME_CHALLENGE=http-01
fi


function create_link {
    local -r source=${1?missing source argument}
    local -r target=${2?missing target argument}
    if [[ -f "$target" ]] && [[ "$(readlink "$target")" == "$source" ]]; then
      set_ownership_and_permissions "$target"
      [[ "$DEBUG" == 1 ]] && echo "$target already linked to $source"
      return 1
    else
      ln -sf "$source" "$target" \
        && set_ownership_and_permissions "$target"
    fi
}

function create_links {
    local -r base_domain=${1?missing base_domain argument}
    local -r domain=${2?missing base_domain argument}

    if [[ ! -f "/etc/nginx/certs/$base_domain/fullchain.pem" || \
          ! -f "/etc/nginx/certs/$base_domain/key.pem" ]]; then
        return 1
    fi
    local return_code=1
    create_link "./$base_domain/fullchain.pem" "/etc/nginx/certs/$domain.crt"
    return_code=$(( return_code & $? ))
    create_link "./$base_domain/key.pem" "/etc/nginx/certs/$domain.key"
    return_code=$(( return_code & $? ))
    if [[ -f "/etc/nginx/certs/dhparam.pem" ]]; then
        create_link ./dhparam.pem "/etc/nginx/certs/$domain.dhparam.pem"
        return_code=$(( return_code & $? ))
    fi
    if [[ -f "/etc/nginx/certs/$base_domain/chain.pem" ]]; then
        create_link "./$base_domain/chain.pem" "/etc/nginx/certs/$domain.chain.pem"
        return_code=$(( return_code & $? ))
    fi
    return $return_code
}

function cleanup_links {
    local -a LETSENCRYPT_CONTAINERS
    local -a LETSENCRYPT_STANDALONE_CERTS
    local -a ENABLED_DOMAINS
    local -a SYMLINKED_DOMAINS
    local -a DISABLED_DOMAINS

    # Create an array containing domains for which a symlinked certificate
    # exists in /etc/nginx/certs (excluding default cert).
    for symlinked_domain in /etc/nginx/certs/*.crt; do
        [[ -L "$symlinked_domain" ]] || continue
        symlinked_domain="${symlinked_domain##*/}"
        symlinked_domain="${symlinked_domain%*.crt}"
        [[ "$symlinked_domain" != "default" ]] || continue
        SYMLINKED_DOMAINS+=("$symlinked_domain")
    done
    [[ "$DEBUG" == 1 ]] && echo "Symlinked domains: ${SYMLINKED_DOMAINS[*]}"

    # Create an array containing domains that are considered
    # enabled (ie present on /app/letsencrypt_service_data or /app/letsencrypt_user_data).
    [[ -f /app/letsencrypt_service_data ]] && source /app/letsencrypt_service_data
    [[ -f /app/letsencrypt_user_data ]] && source /app/letsencrypt_user_data
    LETSENCRYPT_CONTAINERS+=( "${LETSENCRYPT_STANDALONE_CERTS[@]}" )
    for cid in "${LETSENCRYPT_CONTAINERS[@]}"; do
      local -n hosts_array="LETSENCRYPT_${cid}_HOST"
      for domain in "${hosts_array[@]}"; do
        # Add domain to the array storing currently enabled domains.
        ENABLED_DOMAINS+=("$domain")
      done
    done
    [[ "$DEBUG" == 1 ]] && echo "Enabled domains: ${ENABLED_DOMAINS[*]}"

    # Create an array containing only domains for which a symlinked private key exists
    # in /etc/nginx/certs but that no longer have a corresponding LETSENCRYPT_HOST set
    # on an active container or on /app/letsencrypt_user_data
    if [[ ${#SYMLINKED_DOMAINS[@]} -gt 0 ]]; then
        mapfile -t DISABLED_DOMAINS < <(echo "${SYMLINKED_DOMAINS[@]}" \
                                             "${ENABLED_DOMAINS[@]}" \
                                             "${ENABLED_DOMAINS[@]}" \
                                             | tr ' ' '\n' | sort | uniq -u)
    fi
    [[ "$DEBUG" == 1 ]] && echo "Disabled domains: ${DISABLED_DOMAINS[*]}"


    # Remove disabled domains symlinks if present.
    # Return 1 if nothing was removed and 0 otherwise.
    if [[ ${#DISABLED_DOMAINS[@]} -gt 0 ]]; then
      [[ "$DEBUG" == 1 ]] && echo "Some domains are disabled :"
      for disabled_domain in "${DISABLED_DOMAINS[@]}"; do
          [[ "$DEBUG" == 1 ]] && echo "Checking domain ${disabled_domain}"
          cert_folder="$(readlink -f "/etc/nginx/certs/${disabled_domain}.crt")"
          # If the dotfile is absent, skip domain.
          if [[ ! -e "${cert_folder%/*}/.companion" ]]; then
              [[ "$DEBUG" == 1 ]] && echo "No .companion file found in ${cert_folder}. ${disabled_domain} is not managed by certbot-companion. Skipping domain."
              continue
          else
              [[ "$DEBUG" == 1 ]] && echo "${disabled_domain} is managed by certbot-companion. Removing unused symlinks."
          fi

          for extension in .crt .key .dhparam.pem .chain.pem; do
              file="${disabled_domain}${extension}"
              if [[ -n "${file// }" ]] && [[ -L "/etc/nginx/certs/${file}" ]]; then
                  [[ "$DEBUG" == 1 ]] && echo "Removing /etc/nginx/certs/${file}"
                  rm -f "/etc/nginx/certs/${file}"
              fi
          done
      done
      return 0
    else
      return 1
    fi
}

function update_cert {
    local cid="${1:?}"
    local -n hosts_array="LETSENCRYPT_${cid}_HOST"
    local base_domain
    base_domain="$(echo "${hosts_array[@]}" | sed -r 's/ /\n/g' | sort | head -n1)"

    local should_restart_container='false'

    # Base CLI parameters array, used for register, update_account and certonly actions
    # shellcheck disable=SC2155
    local logdir="$(mktemp -d)"
    trap 'rm -rf "$logdir" 2>/dev/null' RETURN
    local -a params_base_arr
    params_base_arr+=(--logs-dir "$logdir")
    params_base_arr+=(--user-agent "certbot-companion/$COMPANION_VERSION (certbot/$CERTBOT_VERSION)")
    [[ "$DEBUG" == 1 ]] && params_base_arr+=(--debug)

    # Alternative trusted root CA path, used for test with Pebble
    if [[ -n "${CA_BUNDLE// }" ]]; then
        if [[ -f "$CA_BUNDLE" ]]; then
            export REQUESTS_CA_BUNDLE="$CA_BUNDLE"
            [[ "$DEBUG" == 1 ]] && echo "Debug: certbot will use $CA_BUNDLE as trusted root CA."
        else
            echo "Warning: the path to the alternate CA bundle ($CA_BUNDLE) is not valid, using default Alpine trust store."
        fi
    fi

    # CLI parameters array used for register and update_account
    local -a params_register_arr

    # CLI parameters array used for certonly
    local -a params_issue_arr
    if [[ "$ACME_CHALLENGE" == "http-01" ]]; then
        params_issue_arr+=(--webroot --webroot-path /usr/share/nginx/html)
    elif [[ "$ACME_CHALLENGE" == "dns-01" ]]; then
        # allow extra parameters, for instance: --authenticator dns-gandi --dns-gandi-credentials $PWD/gandi.ini
        IFS=" " read -r -a acme_dns_params <<< "$ACME_DNS_PARAMS"
        params_issue_arr+=("${acme_dns_params[@]}")
    fi

    local -n cert_keysize="LETSENCRYPT_${cid}_KEYSIZE"
    if [[ -z "$cert_keysize" ]] || \
        # https://neuromancer.sk/std/secg/
        [[ ! "$cert_keysize" =~ ^(2048|3072|4096|ec-256|ec-384|ec-521)$ ]]; then
        cert_keysize=$DEFAULT_KEY_SIZE
    fi
    if [[ "$cert_keysize" = ec-* ]]; then
        params_issue_arr+=(--cert-name "$base_domain" --key-type ecdsa --elliptic-curve "secp${cert_keysize#ec-}r1")
    else
        params_issue_arr+=(--cert-name "$base_domain" --key-type rsa --rsa-key-size "$cert_keysize")
    fi

    # OCSP-Must-Staple extension
    local -n ocsp="ACME_${cid}_OCSP"
    if [[ $(lc "$ocsp") == true ]]; then
        params_issue_arr+=(--must-staple)
    fi

    local -n accountemail="LETSENCRYPT_${cid}_EMAIL"
    local config_home
    # If we don't have a LETSENCRYPT_EMAIL from the proxied container
    # and DEFAULT_EMAIL is set to a non empty value, use the latter.
    if [[ -z "$accountemail" ]]; then
        if [[ -n "${DEFAULT_EMAIL// }" ]]; then
            accountemail="$DEFAULT_EMAIL"
        else
            echo "Error: an email is mandatory to register a certificate."
            return 1
        fi
    fi
    # Got an email, use it with the corresponding config home
    config_home="/etc/certbot/$accountemail"

    local -n acme_ca_uri="ACME_${cid}_CA_URI"
    if [[ -z "$acme_ca_uri" ]]; then
        # Use default or user provided ACME end point
        acme_ca_uri="$ACME_CA_URI"
    fi
    # LETSENCRYPT_TEST overrides LETSENCRYPT_ACME_CA_URI
    local -n test_certificate="LETSENCRYPT_${cid}_TEST"
    if [[ $(lc "$test_certificate") == true ]]; then
        # Use Let's Encrypt ACME V2 staging end point
        acme_ca_uri="$ACME_CA_TEST_URI"
    fi

    # Set relevant --server parameter and ca folder name
    params_base_arr+=(--server "$acme_ca_uri")

    local certificate_dir
    # If we're going to use one of LE stating endpoints ...
    if [[ "$acme_ca_uri" =~ ^https://acme-staging.* ]]; then
        # force config dir to 'staging'
        config_home="/etc/certbot/staging"
        # Prefix test certificate directory with _test_
        certificate_dir="/etc/nginx/certs/_test_$base_domain"
    else
        certificate_dir="/etc/nginx/certs/$base_domain"
    fi
    mkdir -p "$certificate_dir"
    issue_cert_path="${certificate_dir}/cert.pem"
    issue_key_path="${certificate_dir}/key.pem"
    issue_chain_path="${certificate_dir}/chain.pem"
    issue_fullchain_path="${certificate_dir}/fullchain.pem"

    [[ ! -d "$config_home" ]] && mkdir -p "$config_home"
    params_base_arr+=(--config-dir "$config_home" --work-dir "$config_home")

    function get_account() {
        certbot "${params_base_arr[@]}" show_account 2>/dev/null
    }

    # External Account Binding (EAB)
    local -n eab_kid="ACME_${cid}_EAB_KID"
    local -n eab_hmac_key="ACME_${cid}_EAB_HMAC_KEY"
    if ! get_account >/dev/null; then
        if [[ -n "${eab_kid}" && -n "${eab_hmac_key}" ]]; then
            # Register the ACME account with the per container EAB credentials.
            params_register_arr+=(--eab-kid "$eab_kid" --eab-hmac-key "$eab_hmac_key")
        elif [[ -n "${ACME_EAB_KID// }" && -n "${ACME_EAB_HMAC_KEY// }" ]]; then
            # We don't have per-container EAB kid and hmac key or Zero SSL API key.
            # Register the ACME account with the default EAB credentials.
            params_register_arr+=(--eab-kid "$ACME_EAB_KID" --eab-hmac-key "$ACME_EAB_HMAC_KEY")
        else
            # We don't have per container nor default EAB credentials, register a new account.
            params_register_arr+=(--email "$accountemail")
        fi
    fi

    # Zero SSL
    if [[ "$acme_ca_uri" == "https://acme.zerossl.com/v2/DV90" ]]; then
        # Test if we already have:
        #   - an account
        #   - the --email account registration parameter
        #   - the --eab-kid and --eab-hmac-key account registration parameters
        local account_ok='false'
        if get_account >/dev/null; then
            account_ok='true'
        elif in_array '--email' 'params_register_arr'; then
            account_ok='true'
        elif in_array '--eab-kid' 'params_register_arr' && in_array '--eab-hmac-key' 'params_register_arr'; then
            account_ok='true'
        fi

        if [[ $account_ok == 'false' ]]; then
            local -n zerossl_api_key="ZEROSSL_${cid}_API_KEY"
            if [[ -z "$zerossl_api_key" ]]; then
                # Try using the default API key
                zerossl_api_key="${ZEROSSL_API_KEY:-}"
            fi

            if [[ -n "${zerossl_api_key// }" ]]; then
                # Generate a set of ACME EAB credentials using the ZeroSSL API.
                local zerossl_api_response
                if zerossl_api_response="$(curl -s -X POST "https://api.zerossl.com/acme/eab-credentials?access_key=${zerossl_api_key}")"; then
                    if [[ "$(jq -r .success <<< "$zerossl_api_response")" == 'true' ]]; then
                        eab_kid="$(jq -r .eab_kid <<< "$zerossl_api_response")"
                        eab_hmac_key="$(jq -r .eab_hmac_key <<< "$zerossl_api_response")"
                        params_register_arr+=(--eab-kid "$eab_kid" --eab-hmac-key "$eab_hmac_key")
                        [[ "$DEBUG" == 1 ]] && echo "Successfull EAB credentials request against the ZeroSSL API, got the following EAB kid : ${eab_kid}"
                    else
                        # The JSON response body indicated an unsuccesfull API call.
                        echo "Warning: the EAB credentials request against the ZeroSSL API was not successfull."
                    fi
                else
                    # curl failed.
                    echo "Warning: curl failed to make an HTTP POST request to https://api.zerossl.com/acme/eab-credentials."
                fi
            else
                # We don't have a Zero SSL ACME account, EAB credentials, a ZeroSSL API key or an account email :
                # skip certificate account registration and certificate issuance.
                echo "Error: usage of ZeroSSL require an email bound account. No EAB credentials, ZeroSSL API key or email were provided for this certificate, creation aborted."
                return 1
            fi
        fi
    fi

    # Account registration and update if required
    if ! get_account >/dev/null; then
        params_register_arr=("${params_base_arr[@]}" "${params_register_arr[@]}")
        [[ "$DEBUG" == 1 ]] && echo "Calling certbot register -n --agree-tos --no-eff-email with the following parameters : ${params_register_arr[*]}"
        certbot register -n --agree-tos --no-eff-email "${params_register_arr[@]}"
    fi
    if ! get_account | sed -rn '/Email contact/{s/.*: //;p}' | grep -q "$accountemail"; then
        local -a params_update_arr=("${params_base_arr[@]}" --email "$accountemail")
        [[ "$DEBUG" == 1 ]] && echo "Calling certbot update_account -n with the following parameters : ${params_update_arr[*]}"
        certbot update_account -n "${params_update_arr[@]}"
    fi

    # If we still don't have an account by this point, we've got an issue
    if ! get_account >/dev/null; then
        echo "Error: no Certbot account was found or registered for $accountemail and $acme_ca_uri, certificate creation aborted."
        return 1
    fi

    # certbot pre and post hooks
    local -n acme_pre_hook="ACME_${cid}_PRE_HOOK"
    if [[ -n "${acme_pre_hook}" ]]; then
        # Use per-container pre hook
	    params_issue_arr+=(--pre-hook "$acme_pre_hook")
    elif [[ -n ${ACME_PRE_HOOK// } ]]; then
        # Use default pre hook
        params_issue_arr+=(--pre-hook "$ACME_PRE_HOOK")
    fi

    local -n acme_post_hook="ACME_${cid}_POST_HOOK"
    if [[ -n "${acme_post_hook}" ]]; then
        # Use per-container post hook
	    params_issue_arr+=(--post-hook "$acme_post_hook")
    elif [[ -n ${ACME_POST_HOOK// } ]]; then
        # Use default post hook
        params_issue_arr+=(--post-hook "$ACME_POST_HOOK")
    fi

    local -n acme_preferred_chain="ACME_${cid}_PREFERRED_CHAIN"
    if [[ -n "${acme_preferred_chain}" ]]; then
        # Using amce.sh --preferred-chain to select alternate chain.
        params_issue_arr+=(--preferred-chain "$acme_preferred_chain")
    fi
    if [[ "$RENEW_PRIVATE_KEYS" != 'false' ]]; then
        params_issue_arr+=(--new-key)
    fi
    [[ "${2:-}" == "--force-renew" ]] && params_issue_arr+=(--force-renewal)

    # Create directory for the first domain
    mkdir -p "$certificate_dir"
    set_ownership_and_permissions "$certificate_dir"

    for domain in "${hosts_array[@]}"; do
        # Add all the domains to certificate
        params_issue_arr+=(--domain "$domain")
        # Add location configuration for the domain
        add_location_configuration "$domain" || reload_nginx
    done

    params_issue_arr=("${params_base_arr[@]}" "${params_issue_arr[@]}")
    [[ "$DEBUG" == 1 ]] && echo "Calling certbot certonly -n with the following parameters : ${params_issue_arr[*]}"
    echo "Creating/renewal $base_domain certificates... (${hosts_array[*]})"
    certbot certonly -n "${params_issue_arr[@]}"

    local certbot_return=$?

    # 0 = success (ISSUED or SKIP)
    if [[ $certbot_return -eq 0 ]]; then
        if [ -e "/etc/certbot/$accountemail/live/$base_domain/cert.pem" ]; then
            cp -v "/etc/certbot/$accountemail/live/$base_domain/cert.pem" "${issue_cert_path}"
            cp -v "/etc/certbot/$accountemail/live/$base_domain/privkey.pem" "${issue_key_path}"
            cp -v "/etc/certbot/$accountemail/live/$base_domain/chain.pem" "${issue_chain_path}"
            cp -v "/etc/certbot/$accountemail/live/$base_domain/fullchain.pem" "${issue_fullchain_path}"
        fi
        for domain in "${hosts_array[@]}"; do
            if [[ $acme_ca_uri =~ ^https://acme-staging.* ]]; then
                create_links "_test_$base_domain" "$domain" \
                    && should_reload_nginx='true' \
                    && should_restart_container='true'
            else
                create_links "$base_domain" "$domain" \
                    && should_reload_nginx='true' \
                    && should_restart_container='true'
            fi
        done
        echo "${COMPANION_VERSION:-}" > "${certificate_dir}/.companion"
        set_ownership_and_permissions "${certificate_dir}/.companion"
        # Make private key root readable only
        for file in cert.pem key.pem chain.pem fullchain.pem; do
            local file_path="${certificate_dir}/${file}"
            [[ -e "$file_path" ]] && set_ownership_and_permissions "$file_path"
        done
        # shellcheck disable=SC2155
        local acme_private_key="/etc/certbot/$accountemail/live/$base_domain/privkey.pem"
        [[ -e "$acme_private_key" ]] && set_ownership_and_permissions "$acme_private_key"
        while read -r file; do
            set_ownership_and_permissions "$file"
        done <<< "$(find "/etc/certbot/$accountemail/archive" -type f -name "privkey*.pem")"
        # Queue nginx reload if a certificate was issued or renewed
        should_reload_nginx='true'
        should_restart_container='true'
    fi

    # Restart container if certs are updated and the respective environmental variable is set
    local -n restart_container="LETSENCRYPT_${cid}_RESTART_CONTAINER"
    if [[ $(lc "$restart_container") == true ]] && [[ "$should_restart_container" == 'true' ]]; then
        echo "Restarting container (${cid})..."
        docker_restart "${cid}"
    fi

    for domain in "${hosts_array[@]}"; do
        if [[ -f "/etc/nginx/conf.d/standalone-cert-$domain.conf" ]]; then
            [[ "$DEBUG" == 1 ]] && echo "Debug: removing standalone configuration file /etc/nginx/conf.d/standalone-cert-$domain.conf"
            rm -f "/etc/nginx/conf.d/standalone-cert-$domain.conf" && should_reload_nginx='true'
        fi
    done

    if ! parse_true "${RELOAD_NGINX_ONLY_ONCE:-false}" && parse_true $should_reload_nginx; then
        reload_nginx
    fi
}

function update_certs {
    local -a LETSENCRYPT_CONTAINERS
    local -a LETSENCRYPT_STANDALONE_CERTS

    pushd /etc/nginx/certs > /dev/null || return
    check_nginx_proxy_container_run || return

    # Load relevant container settings
    if [[ -f /app/letsencrypt_service_data ]]; then
        source /app/letsencrypt_service_data
    else
        echo "Warning: /app/letsencrypt_service_data not found, skipping data from containers."
    fi

    # Load settings for standalone certs
    if [[ -f /app/letsencrypt_user_data ]]; then
        if source /app/letsencrypt_user_data; then
            for cid in "${LETSENCRYPT_STANDALONE_CERTS[@]}"; do
                local -n hosts_array="LETSENCRYPT_${cid}_HOST"
                for domain in "${hosts_array[@]}"; do
                    add_standalone_configuration "$domain"
                done
            done
            reload_nginx
            LETSENCRYPT_CONTAINERS+=( "${LETSENCRYPT_STANDALONE_CERTS[@]}" )
        else
            echo "Warning: could not source /app/letsencrypt_user_data, skipping user data"
        fi
    fi

    should_reload_nginx='false'
    for cid in "${LETSENCRYPT_CONTAINERS[@]}"; do
        # Pass the eventual --force-renew arg to update_cert() as second arg
        update_cert "$cid" "${1:-}"
    done

    cleanup_links && should_reload_nginx='true'

    [[ "$should_reload_nginx" == 'true' ]] && reload_nginx

    popd > /dev/null || return
}

# Allow the script functions to be sourced without starting the Service Loop.
if [ "${1}" == "--source-only" ]; then
  return 0
fi

pid=
# Service Loop: When this script exits, start it again.
trap '[[ $pid ]] && kill $pid; exec $0' EXIT
trap 'trap - EXIT' INT TERM

update_certs "$@"

# Wait some amount of time
echo "Sleep for ${CERTS_UPDATE_INTERVAL}s"
sleep "$CERTS_UPDATE_INTERVAL" & pid=$!
wait
pid=
